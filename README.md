# Status Bar Module Blocklets

System status module blocklets written in bash for system information on status bar.

## Dependencies

- Gnu Coreutils [Learn more](https://www.gnu.org/software/coreutils/)
- jq [website](https://stedolan.github.io/jq/)

## Getting started
These blocklets can be used in any bar or panel such as `Polybar` `i3Blocks`

## Installation

This command will clone this repo to your .config folder with folder name Blocklets.

```
git clone https://gitlab.com/freezboltz/status-bar-module-blocklets.git ~/.config/Blocklets
```
go to directory `~/.config/Blocklets` and check if all file within it has executable permissions
if not make it executable
##### permission schemes
```
rwxr-xr-x owner owner file
```
## Implementation on i3Block

To your i3blocks.conf file edit as follows:

```
# file: i3blocks.conf

# ... some code ...

[net]
command=$HOME/.config/Blocklets/moduleBlocklets/sb-nettraf
interval=5
color=#fff

[moonphase]
command=$HOME/.config/Blocklets/moduleBlocklets/sb-moonphase
interval=once

[weather]
command=$HOME/.config/Blocklets/moduleBlocklets/sb-weather
interval=once

[memory]
command=$HOME/.config/Blocklets/moduleBlocklets/sb-memory
interval=5
color=#fff

[cpu]
command=$HOME/.config/Blocklets/moduleBlocklets/sb-cpu
interval=10
color=#fff

[volume]
interval=once
command=$HOME/.config/Blocklets/moduleBlocklets/sb-volume
signal=10
color=#fff

[disk]
color=#fff
command=$HOME/.config/Blocklets/moduleBlocklets/sb-disk
interval=10

[calendar]
command=$HOME/.config/Blocklets/moduleBlocklets/sb-calender
interval=1
LABEL=
DATEFMT=+%a %d.%m.%Y %H:%M:%S
HEIGHT=180
WIDTH=220
color=#fff
```
## Polybar
- [refer to this document](https://github.com/polybar/polybar/wiki/Module:-script)

## dwmblocks
- [refer to this source](https://raw.githubusercontent.com/torrinfail/dwmblocks/master/blocks.def.h)
